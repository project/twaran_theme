<?php

function twaran_theme_js_alter(&$js) {
  if (isset($js['misc/jquery.js'])) {
    $js['misc/jquery.js']['data'] = drupal_get_path('theme', 'twaran_theme') . '/bower_components/jquery/dist/jquery.js';
    $js['misc/jquery.js']['version'] = '2.1.4';
  }
}
function twaran_theme_preprocess_page(&$variables) {

// Convenience variables
  if (!empty($variables['page']['sidebar_left_full_one'])){
    $left = $variables['page']['sidebar_left_full_one'];
  }

  if (!empty($variables['page']['sidebar_right_full'])) {
    $right = $variables['page']['sidebar_right_full'];
  }

  // Dynamic sidebars
  if (!empty($left) && !empty($right)) {
  	$variables['main_grid_custom'] = 'large-6';
    $variables['sidebar_first_grid'] = 'large-3';
    $variables['sidebar_sec_grid'] = 'large-3';
  } elseif (empty($left) && !empty($right)) {
  	$variables['main_grid_custom'] = 'large-9';
    $variables['sidebar_first_grid'] = '';
    $variables['sidebar_sec_grid'] = 'large-3';
  } elseif (!empty($left) && empty($right)) {
  	$variables['main_grid_custom'] = 'large-9';
    $variables['sidebar_first_grid'] = 'large-3';
    $variables['sidebar_sec_grid'] = '';
  } else {
  	$variables['main_grid_custom'] = 'large-12 grid-only';
    $variables['sidebar_first_grid'] = '';
    $variables['sidebar_sec_grid'] = '';
  }
}
